package com.centroid.travalogue.app;

import android.content.Context;

import androidx.multidex.MultiDexApplication;

public class TravalogueApp extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }
}
