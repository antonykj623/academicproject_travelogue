package com.centroid.travalogue.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.centroid.travalogue.R;

public class HomeActivity extends AppCompatActivity {

    ImageView imgperson;

    RecyclerView recyclerview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getSupportActionBar().hide();

        imgperson=findViewById(R.id.imgperson);
        recyclerview=findViewById(R.id.recyclerview);


        imgperson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                startActivity(new Intent(HomeActivity.this,UserProfileActivity.class));

            }
        });
    }
}
