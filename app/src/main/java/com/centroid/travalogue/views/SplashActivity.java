package com.centroid.travalogue.views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.centroid.travalogue.Constants.AppConstants;
import com.centroid.travalogue.PreferenceHelper.PreferenceHelper;
import com.centroid.travalogue.R;

public class SplashActivity extends AppCompatActivity {

    Handler  handler=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                if(new PreferenceHelper(SplashActivity.this).getData(AppConstants.UserID).equals(""))
                {


                    startActivity(new Intent(SplashActivity.this,LoginActivity.class));

                }
                else {

                    startActivity(new Intent(SplashActivity.this,HomeActivity.class));

                }



            }
        },3000);
    }
}
